#Test K means 1 dimensi


# import array as arr # Import array lib
import random # Import random lib
import numpy as np

# inisiasi data
x= [12,14,3,5,10,11,2,7,9,21]

cluster= int(input("Masukan jumlah klaster: "))

# example centroid
# centroid= [3,11]
centroid= random.sample(list(x),k=cluster) #get random value from array x
print("Pusat Klaster awal: "+str(centroid)+"\n")
# print(rnd)
[float(i) for i in centroid]
old_centroid=[] #temp for old centroid
# [float(i) for i in old_centroid]


# create list for member of centroid
member_of_centroid = [[] for i in range(len(centroid))]

# print(member_of_centroid)

condition= "not convergen"

def kMeans(x, centroid, old_centroid, member_of_centroid):
    del member_of_centroid[:]    
    # create list for member of centroid
    member_of_centroid = [[] for i in range(len(centroid))]
    # initiate valeue of means
    distance= []

    # start process
    for data_x in range(len(x)):
        # print(x[data_x])
        j=0
        for data_centroid in range(len(centroid)):
            # print(centroid[data_centroid])
            # get value of means for every  centroid
            distance.insert(j,np.sqrt((x[data_x] - centroid[data_centroid]) ** 2))   
            # print(distance[j], centroid[data_centroid], x[data_x], j)
            j= j+1    

        # get minimum value of means from centroid
        mini= min(distance)
        # print(mini)
        del distance[:] # clear value means data

        for data_centroid in range(len(centroid)):
            nilai_mini= np.sqrt((x[data_x] - centroid[data_centroid]) ** 2)
            if mini == nilai_mini:
                # print("data: "+ str(x[data_x]) + " anggota cluster: "+ str(centroid[data_centroid]) + " Iter: "+ str(data_centroid))
                member_of_centroid[data_centroid].insert(len(member_of_centroid[data_centroid]), x[data_x])

    # print(centroid)
    # print("Klaster ke "+ str(i+1)+ " "+str(member_of_centroid))        

    # copy centroid to temp old_centroid
    old_centroid= centroid.copy()
    # print(old_centroid)

    return (centroid, old_centroid, member_of_centroid)


while condition== "not convergen":
    centroid, old_centroid, member_of_centroid= kMeans(x, centroid, old_centroid, member_of_centroid)
    # get new centroid
    for i in range(len(centroid)):
        new_centroid= sum(member_of_centroid[i])/len(member_of_centroid[i])
        centroid[i]= new_centroid
        print("Klaster ke "+ str(i+1)+ ": "+str(member_of_centroid[i])) 
        print("Pusat klaster ke "+ str(i+1)+ ": "+str(centroid[i])+ "\n")

    if old_centroid==centroid:
        condition= "convergen"

    del member_of_centroid[:]    
    # create list for member of centroid
    member_of_centroid = [[] for i in range(len(centroid))]


    print(condition+ "\n" + "=================")
    kMeans(x, centroid, old_centroid, member_of_centroid) # use K-Means function
    