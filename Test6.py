# Test SSE

data= [[2.0, 3.0], [10.0, 20.0], [8.0, 17.0], [7.5, 20.5], [24.0, 4.0], [11.5, 20.5], [21.5, 25.0], [2.5, 25.5]]

temp_x=[]
temp_y=[]
for i in range(len(data)):
    temp_x.insert(i, data[i][0])
    temp_y.insert(i, data[i][1])

# get average
avg_x= sum(temp_x)/len(temp_x) 
avg_y= sum(temp_y)/len(temp_y)

post_data=[]
for j in range(len(data)):
    post_data.insert(j,((data[j][0]-avg_x)**2)+((data[j][1]-avg_y)**2))

sse= sum(post_data)    
print(sse)