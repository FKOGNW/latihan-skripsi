# Test ploting member of centroid


import random  # Import random lib
import time  # knowing running time program
from time import sleep

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
# import progressbar


def findMemberCentroid(data, centroid):
    
    member_of_centroid = [[] for i in range(len(centroid))]

    distance=[]
    temp_centroid_count= 0
    temp_member=[]

    # bar = progressbar.ProgressBar(maxval=20, \
    # widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    # bar.start()
    # print(itera)
    for data_count in range(len(data)):
        mini=9999999999999999
        j=0
        for centroid_count in range(len(centroid)):
            # Get distance for each data to centroid
            distance.insert(j, np.sqrt(((data[data_count][0] - centroid[centroid_count][0]) ** 2) + ((data[data_count][1] - centroid[centroid_count][1]) ** 2)))
            
            # checking distance between centroid
            if distance[j]< mini:
                # update minimum temp value
                mini= distance[j]
                
                # get minimum distance to mark centroid
                temp_member= data[data_count]
                temp_centroid_count= centroid_count
            j+= 1
        # itera+=1    
        
        # assign member of centroid
        member_of_centroid[temp_centroid_count].insert(len(member_of_centroid[temp_centroid_count]), temp_member)
        
        # bar.update(data_count+1)
        # sleep(0.1)
    # bar.finish()
    return (member_of_centroid)

def findCentroid (data, total_centroid):   
    random.seed(200) # using seed to get constant value
    centroid= random.sample(list(data), k=total_centroid) # Get random valur from list   

    old_centroid=[[]]

    condition="not convergen"
    # print(member_of_centroid)
    max_loop=0
    while condition== "not convergen" or max_loop==1000:
        # print('.')
        member_of_centroid= findMemberCentroid(data, centroid)

        # copying centroid to old centroid
        old_centroid= centroid.copy()      
        # print(old_centroid)
        temp_x_centroid=[[] for i in range(len(old_centroid))]
        temp_y_centroid=[[] for i in range(len(old_centroid))]
        for i in range(len(old_centroid)):
            # k=0
            for j in range(len(member_of_centroid[i])):
                # print(member_of_centroid[i][j][0])
                temp_x_centroid[i].insert(j,member_of_centroid[i][j][0])
                temp_y_centroid[i].insert(j,member_of_centroid[i][j][1])
                 
                # calculate average to get new centroid 
                temp_x= sum(temp_x_centroid[i])/len(temp_x_centroid[i])
                temp_y= sum(temp_y_centroid[i])/len(temp_y_centroid[i])
                centroid[i]= [temp_x,temp_y]
                # print(old_centroid)
                # centroid[i][1]= temp_y

            #     # print(str(old_centroid)+ str(i)) 
            #      
            # print("Klaster ke "+ str(i+1)+ ": "+str(member_of_centroid[i])) 
            # print("Pusat klaster "+ str(i+1)+" baru "+  ": "+str(centroid[i])+ "\n")
        # print(old_centroid)
        print("Start iteration "+ str(max_loop+1)+"....")
        max_loop+=1    
        if old_centroid== centroid or max_loop== 1000:
            condition= "convergen"
            print("==========\n" + condition)
            return centroid, member_of_centroid

df_loc= pd.read_csv("data/databike.csv", dtype={"start station latitude": float, "start station longitude": float})
df_loc.head()

X = df_loc[['latitude', 'longitude']].values
data= X.tolist()
# print(X.tolist())
# print(len(X))

centroid, member_of_centroid= findCentroid(data, 3)
print(centroid)   
# print(len(member_of_centroid[0]))

centroid= np.array(centroid)


member_centroid_1= np.array(member_of_centroid[0])
member_centroid_2= np.array(member_of_centroid[1])
member_centroid_3= np.array(member_of_centroid[2])
# member_centroid_4= np.array(member_of_centroid[3])
# member_centroid_5= np.array(member_of_centroid[4])
# member_centroid_6= np.array(member_of_centroid[5])
# member_centroid_7= np.array(member_of_centroid[6])
# member_centroid_8= np.array(member_of_centroid[7])

plt.scatter(member_centroid_1[:, 0],member_centroid_1[:, 1], marker='o', s=100,
            c='r', label='cluster 1')
plt.scatter(member_centroid_2[:, 0],member_centroid_2[:, 1], marker='*', s=100,
            c='b', label='cluster 2')
plt.scatter(member_centroid_3[:, 0],member_centroid_3[:, 1], marker='^', s=100,
            c='y', label='cluster 3')
# plt.scatter(member_centroid_4[:, 0],member_centroid_4[:, 1], marker='o', s=100,
#             c='b', label='cluster 4')
# plt.scatter(member_centroid_5[:, 0],member_centroid_5[:, 1], marker='*', s=100,
#             c='y', label='cluster 5')
# plt.scatter(member_centroid_6[:, 0],member_centroid_6[:, 1], marker='^', s=100,
#             c='r', label='cluster 6')
# plt.scatter(member_centroid_7[:, 0],member_centroid_7[:, 1], marker='o', s=100,
#             c='y', label='cluster 7')
# plt.scatter(member_centroid_8[:, 0],member_centroid_8[:, 1], marker='*', s=100,
            # c='b', label='cluster 8')
# plt.scatter(centroid[0][0],centroid[0][1], marker='*', s=150,
            # c='g', label='centroid')
# plt.scatter(centroid[1][0],centroid[1][1], marker='*', s=150,
#             c='g', label='centroid')
plt.scatter(centroid[:, 0], centroid[:, 1], marker='*', s=150,
            c='g', label='centroid')

plt.legend()
plt.xlabel('latitude')
plt.ylabel('longitude')
plt.title('Visualization of clustered data', fontweight='bold')
plt.show()
