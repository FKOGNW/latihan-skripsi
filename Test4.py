# Test K Means 2 dimensi (array)


import random # Import random lib
import numpy as np



# print(old_centroid)



# create list for member of centroid
# member_of_centroid= [[] for i in range(len(centroid))]
# print(member_of_centroid)
def findMemberCentroid(data, centroid):
    # del member_of_centroid[:]    
    # create list for member of centroid
    member_of_centroid = [[] for i in range(len(centroid))]

    distance=[]
    for data_count in range(len(data)):
        mini=10000000000000000000 
        j=0
        for centroid_count in range(len(centroid)):
            distance.insert(j, np.sqrt(((data[data_count][0] - centroid[centroid_count][0]) ** 2) + ((data[data_count][1] - centroid[centroid_count][1]) ** 2)))
            # checking distance between centroid
            if distance[j]< mini:
                mini= distance[j]
                temp_member= data[data_count]
                temp_centroid_count= centroid_count
            j+= 1
        
        # assign member of centroid
        member_of_centroid[temp_centroid_count].insert(len(member_of_centroid[temp_centroid_count]), temp_member)

        del distance[:] # clear data distance    
        # get minimum value of means from centroid
        # mini= min(distance)
        # print(data[data_count])
        # print(distance )

        # assign member of centroid
        # for centroid_count in range(len(centroid)):
        #     minimum= np.sqrt(((data[data_count][0] - centroid[centroid_count][0]) ** 2) + ((data[data_count][1] - centroid[centroid_count][1]) ** 2))

        #     if mini== minimum:
        #         member_of_centroid[centroid_count].insert(len(member_of_centroid[centroid_count]), data[data_count])

    # copy centroid to temp old centroid
    # print(member_of_centroid[1][0])
    # print(member_of_centroid[0])
    return (member_of_centroid)

def updateCentroid(data, centroid):
    old_centroid=[[]]
    condition="not convergen"
    while condition== "not convergen":
        member_of_centroid= findMemberCentroid(data, centroid)

        old_centroid= centroid.copy()      
        # print(old_centroid)
        temp_x_centroid=[[] for i in range(len(old_centroid))]
        temp_y_centroid=[[] for i in range(len(old_centroid))]
        for i in range(len(old_centroid)):
            # k=0
            for j in range(len(member_of_centroid[i])):
                # print(member_of_centroid[i][j][0])
                temp_x_centroid[i].insert(j,member_of_centroid[i][j][0])
                temp_y_centroid[i].insert(j,member_of_centroid[i][j][1])
                # temp_centroid_x[i].insert(j,member_of_centroid[i][j][0])
                
            # print(temp_x_centroid[i])
            # print(temp_y_centroid[i])
            # print("===="+ str(i))
        # for i in range(len(old_centroid)):    
                temp_x= sum(temp_x_centroid[i])/len(temp_x_centroid[i])
                temp_y= sum(temp_y_centroid[i])/len(temp_y_centroid[i])
                centroid[i]= [temp_x,temp_y]
                # print(old_centroid)
                # centroid[i][1]= temp_y

                # print(str(old_centroid)+ str(i))      
            print("Klaster ke "+ str(i+1)+ ": "+str(member_of_centroid[i])) 
            print("Pusat klaster "+ str(i+1)+" baru "+  ": "+str(centroid[i])+ "\n")
        # print(old_centroid)
        if old_centroid== centroid:
            condition= "convergen"

        # del member_of_centroid[:]    
        # # create list for member of centroid
        # member_of_centroid = [[] for i in range(len(centroid))]


        print(condition+ "\n" + "=================")
        # findMemberCentroid(data, centroid) # use K-Means function

# inisiasi data
data= [[2,3], [10,20], [24,4], [12,21], [7,20], [8,21], [20,19], [1,21], [11,20], [23,31], [8,17], [4,30]]

cluster= int(input("Masukan jumlah klaster: "))

# example centroid
# centroid=[[24,4], [4,30]]
centroid= random.sample(list(data), k=cluster) # Get random valur from array

# delete same data with initiate centroid
# for i in range(len(centroid)):
#     data.remove(centroid[i])  

print(data)        
# print(centroid)
# print(centroid[0][0])

for i in range(len(centroid)):
    print("Pusat klaster awal ke "+ str(i+1) + " "+str(centroid[i]))
print("\n")

updateCentroid(data, centroid)        
print(centroid)
