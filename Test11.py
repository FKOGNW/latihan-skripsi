import numpy as np 
import matplotlib.pyplot as plt
from sklearn import metrics
from sklearn.cluster import KMeans



data= [[5,3], [10,15], [24,4], [12,21], [7,20], [8,21], [20,19], [1,21], [11,20], [23,31], [8,17], [4,30],
        [10,9], [21,23], [9,19], [9,8], [22,11], [20,9], [9,12], [21,4]]

score= []
range_values= np.arange(2,11)

for i in range_values:
    kmeans= KMeans(init='k-means++', n_clusters=i, n_init=10)
    kmeans.fit(data)

    score= metrics.silhouette_score(data, kmeans.labels_, metric='euclidean', sample_size=(len(data)))

    print('number of cluster= ', i)
    print('score: ', score)