import time # knowing running time program

import random # Import random lib
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from statistics import mean 


def findMemberCentroid(data, centroid):
    
    member_of_centroid = [[] for i in range(len(centroid))]

    distance=[]
    temp_centroid_count= 0
    temp_member=[]
    # print(itera)
    for data_count in range(len(data)):
        mini=9999999999999999
        j=0
        for centroid_count in range(len(centroid)):
            # Get distance for each data to centroid
            distance.insert(j, np.sqrt(((data[data_count][0] - centroid[centroid_count][0]) ** 2) + ((data[data_count][1] - centroid[centroid_count][1]) ** 2)))
            
            # checking distance between centroid
            if distance[j]< mini:
                # update minimum temp value
                mini= distance[j]
                
                # get minimum distance to mark centroid
                temp_member= data[data_count]
                temp_centroid_count= centroid_count
            j+= 1
        # itera+=1    
        
        # assign member of centroid
        member_of_centroid[temp_centroid_count].insert(len(member_of_centroid[temp_centroid_count]), temp_member)

    return (member_of_centroid)

def findCentroid (data, total_centroid):   
    random.seed(200) # using seed to get constant value
    centroid= random.sample(list(data), k=total_centroid) # Get random valur from list   

    old_centroid=[[]]

    condition="not convergen"
    # print(member_of_centroid)
    max_loop=0
    while condition== "not convergen" or max_loop==1000:
        # print('.')
        member_of_centroid= findMemberCentroid(data, centroid)

        # copying centroid to old centroid
        old_centroid= centroid.copy()      
        # print(old_centroid)
        temp_x_centroid=[[] for i in range(len(old_centroid))]
        temp_y_centroid=[[] for i in range(len(old_centroid))]
        for i in range(len(old_centroid)):
            # k=0
            for j in range(len(member_of_centroid[i])):
                # print(member_of_centroid[i][j][0])
                temp_x_centroid[i].insert(j,member_of_centroid[i][j][0])
                temp_y_centroid[i].insert(j,member_of_centroid[i][j][1])
                 
                # calculate average to get new centroid 
                temp_x= sum(temp_x_centroid[i])/len(temp_x_centroid[i])
                temp_y= sum(temp_y_centroid[i])/len(temp_y_centroid[i])
                centroid[i]= [temp_x,temp_y]
                # print(old_centroid)
                # centroid[i][1]= temp_y

            #     # print(str(old_centroid)+ str(i)) 
            #      
            # print("Klaster ke "+ str(i+1)+ ": "+str(member_of_centroid[i])) 
            # print("Pusat klaster "+ str(i+1)+" baru "+  ": "+str(centroid[i])+ "\n")
        # print(old_centroid)
        print("Iteration "+ str(max_loop+1), end="\r")
        max_loop+=1    
        if old_centroid== centroid or max_loop== 1000:
            condition= "convergen"
            print("Done with "+ str(max_loop+1) + " iteration\n" + condition + "\n==========\n")
            return centroid, member_of_centroid

def getSse(centroid, member_of_centroid):
    sse=0
    temp_sse= []
    for i in range(len(centroid)):
        distance_x= []
        distance_y= []
        average_x= 0
        average_y= 0
        for j in range(len(member_of_centroid[i])):
            distance_x.insert(j, np.sqrt(((member_of_centroid[i][j][0] - centroid[i][0]) ** 2)))
            distance_y.insert(j, np.sqrt(((member_of_centroid[i][j][1] - centroid[i][1]) ** 2)))
        average_x= mean(distance_x)
        average_y= mean(distance_y)
        
        temp= 0
        for k in range(len(member_of_centroid[i])):
            # print(distance_y)
            # print((member_of_centroid[i][k][0]- average_x)**2 )
            temp+= ((distance_x[k]- average_x)**2 + (distance_y[k]- average_y)**2)
        sse+= temp

    return sse    

    
    # for i in range(len(centroid)):
    #     # print(distance[i])
    #     # print(sse)
    #     sse+= pow(centroid[i]- average,2)
    #     print(centroid[i]- average)
    #     print('sse',sse)

    # print(sse)
    # print(average)
    # return sse

def getRecommendCluster(sse):
    difference= []
    for i in range(len(sse)):
        if i != len(sse)-1:
            difference.insert(i, sse[i+1]- sse[i])
            # print(str(i)+ str(difference))
    # print(sse)
    # print(difference)
    # print(max(difference))
    # plt.scatter(difference)
    # plt.show()
    # print(difference.index(max(difference))+1)

    # knowing biggest difference in deviation
    recommend_cluster= difference.index(max(difference))+1

    return recommend_cluster


df_loc= pd.read_csv("data/databike.csv", dtype={"start station latitude": float, "start station longitude": float})
df_loc.head()

X = df_loc[['latitude', 'longitude']].values
data= X.tolist()
# print(X.tolist())
# print(len(X))

# centroid= findCentroid(data, 2)
# print(centroid)

sse=[]
temp_centroid=[]
for i in range(10):
    # print(centroid)
    centroid, member_of_centroid= findCentroid(data, i+1)    
    # print("Pusat dengan " + str(i+1) + " klaster :"+str(centroid))
    # temp_centroid.insert(i,centroid[:])
    temp_sse= getSse(centroid, member_of_centroid)
    sse.insert(i,temp_sse)

elbow= getRecommendCluster(sse)
# print(sse)
print(elbow)

# print(findCentroid(data, elbow) )

start_time = time.time()
print("--- %s seconds ---" % (time.time() - start_time)) 
  
plt.plot([1,2,3,4,5,6,7,8,9,10],sse, '-ro')

plt.xlabel("Jumlah cluster")
plt.ylabel("SSE")
plt.show()