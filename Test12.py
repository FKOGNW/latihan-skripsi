# Trying elbow

import time # knowing running time program

import random # Import random lib
import numpy as np
import matplotlib.pyplot as plt
from statistics import mean 
import math



# print(member_of_centroid)
def findMemberCentroid(data, centroid):
    # del member_of_centroid[:]    
    # create list for member of centroid
    member_of_centroid = [[] for i in range(len(centroid))]

    distance=[]
    for data_count in range(len(data)):
        mini=9999999999999999
        j=0
        for centroid_count in range(len(centroid)):
            # Get distance for each data to centroid
            distance.insert(j, np.sqrt(((data[data_count][0] - centroid[centroid_count][0]) ** 2) + ((data[data_count][1] - centroid[centroid_count][1]) ** 2)))
            
            # checking distance between centroid
            if distance[j]< mini:
                # update minimum temp value
                mini= distance[j]
                
                # get minimum distance to mark centroid
                temp_member= data[data_count]
                temp_centroid_count= centroid_count
            j+= 1
        
        # assign member of centroid
        member_of_centroid[temp_centroid_count].insert(len(member_of_centroid[temp_centroid_count]), temp_member)

    return (member_of_centroid)

def findCentroid (data, total_centroid):   
    random.seed(200) # using seed to get constant value
    centroid= random.sample(list(data), k=total_centroid) # Get random valur from list   
    # centroid=total_centroid
    old_centroid=[[]]

    condition="not convergen"
    # print(member_of_centroid)

    while condition== "not convergen":
        member_of_centroid= findMemberCentroid(data, centroid)

        # copying centroid to old centroid
        old_centroid= centroid.copy()      
        # print(old_centroid)
        temp_x_centroid=[[] for i in range(len(old_centroid))]
        temp_y_centroid=[[] for i in range(len(old_centroid))]
        for i in range(len(old_centroid)):
            # k=0
            for j in range(len(member_of_centroid[i])):
                # print(member_of_centroid[i][j][0])
                temp_x_centroid[i].insert(j,member_of_centroid[i][j][0])
                temp_y_centroid[i].insert(j,member_of_centroid[i][j][1])
                # calculate average to get new centroid 
                temp_x= sum(temp_x_centroid[i])/len(temp_x_centroid[i])
                temp_y= sum(temp_y_centroid[i])/len(temp_y_centroid[i])
                centroid[i]= [temp_x,temp_y]
                # print(old_centroid)
                # centroid[i][1]= temp_y

            #     # print(str(old_centroid)+ str(i)) 
            #      
            # print("Klaster ke "+ str(i+1)+ ": "+str(member_of_centroid[i])) 
            # print("Pusat klaster "+ str(i+1)+" baru "+  ": "+str(centroid[i])+ "\n")
        # print(old_centroid)
        if old_centroid== centroid:
            condition= "convergen"
            return centroid, member_of_centroid
            # print(condition+ "\n" + "=================")

            # return centroid
       
            

            # print(condition+ "\n" + "=================")
            # findMemberCentroid(data, centroid ) # use K-Means function
            # print(member_of_centroid)

def getSse(data, centroid, member_of_centroid):
    print(len(centroid))
    sse=0
    # for i in range(len(centroid)):
    #     print('cen',i)
    #     distance=[]
    #     print(distance)
    #     average=0
    #     for j in range(len(member_of_centroid[i])):
    #         print('j',j)
    #         # Get mean
    #         distance.insert(j, np.sqrt(((member_of_centroid[i][j][0] - centroid[i][0]) ** 2) + ((member_of_centroid[i][j][1] - centroid[i][1]) ** 2)))
    #     print(distance)
    #     average= mean(distance)
    #     print(average)

    #     for k in range(len(member_of_centroid[i])):
    #         sse+= (distance[k]- average)**2

    #     print(sse)
    # distance=[]
    # for i in range(len(centroid)):
    #     for j in range(len(data)):
    #         print(data[j][0])
    #         distance.insert(j, np.sqrt(((data[j][0] - centroid[i][0]) ** 2) + ((data[j][1] - centroid[i][1]) ** 2)))
    #     average= mean(distance)
    #     print(average)

    #     distance=[]
    #     for k in range(len(member_of_centroid[i])):
    #         distance.insert(j, np.sqrt(((member_of_centroid[i][k][0] - centroid[i][0]) ** 2) + ((member_of_centroid[i][k][1] - centroid[i][1]) ** 2)))
    #         sse+= (distance[k]- average)**2

    # return sse    

    #### Almost ###
    # temp_sse= []
    # for i in range(len(centroid)):
    #     distance_x= []
    #     distance_y= []
    #     average_x= 0
    #     average_y= 0
    #     for j in range(len(member_of_centroid[i])):
    #         distance_x.insert(j, np.sqrt(((member_of_centroid[i][j][0] - centroid[i][0]) ** 2)))
    #         distance_y.insert(j, np.sqrt(((member_of_centroid[i][j][1] - centroid[i][1]) ** 2)))
    #     average_x= mean(distance_x)
    #     average_y= mean(distance_y)
        
    #     temp= 0
    #     for k in range(len(member_of_centroid[i])):
    #         print(distance_y)
    #         print((member_of_centroid[i][k][0]- average_x)**2 )
    #         temp+= ((distance_x[k]- average_x)**2 + (distance_y[k]- average_y)**2)
    #     sse+= temp

    

    deviation_x= []
    deviation_y= []
    for i in range(len(centroid)):
        temp_x= []
        temp_y= []

        average_x= 0
        average_y= 0
        for j in range(len(member_of_centroid[i])):
            temp_x.insert(j, member_of_centroid[i][j][0])
            temp_y.insert(j, member_of_centroid[i][j][1])
        average_x= mean(temp_x)    
        average_y= mean(temp_y)    
        print(average_x)

        temp= 0
        for k in range(len(member_of_centroid[i])):
            print((member_of_centroid[i][k][0]- average_x)**2 )
            temp+= (((member_of_centroid[i][k][0]- average_x)+(member_of_centroid[i][k][1]- average_y))**2)
            # temp+= ((member_of_centroid[i][k][1]- average_y)**2)
        sse= sse+ temp

    return sse    
    # sse= math.fsum(temp_sse)    
    # for i in range(len(centroid)):
    #     # print(distance[i])
    #     # print(sse)
    #     sse+= pow(centroid[i]- average,2)
    #     print(centroid[i]- average)
    #     print('sse',sse)

    # print(sse)
    # print(average)
    # return sse

def getRecommendCluster(sse):
    difference= []
    for i in range(len(sse)):
        if i != len(sse)-1:
            difference.insert(i, sse[i]- sse[i+1])
            print(str(i)+ str(difference))
    # print(sse)
    # print(difference)
    # print(max(difference))
    # print(difference.index(max(difference))+1)

    # knowing biggest difference in deviation
    recommend_cluster= difference.index(max(difference))+1

    return recommend_cluster

# inisiasi data

data= [[5,3], [10,15], [24,4], [12,21], [7,20], [8,21], [20,19], [1,21], [11,20], [23,31], [8,17], [4,30],
        [10,9], [21,23], [9,19], [9,8], [22,11], [20,9], [9,12], [21,4]]

# data= [[5,3], [10,15], [24,4], [12,21], [7,20], [8,21], [20,19], [1,21], [11,20], [23,31], [8,17], [4,30],
#         [10,9], [21,23], [3,8], [1,2], [27,17], [23,19], [5,10], [13,3]]
# # example centroid
# centro=[[21,3], [5,3]] # 2 Centroid
# centroid, member_of_centroid= findCentroid(data, centro)
# temp_sse= getSse(centroid, member_of_centroid)

# print(temp_sse)
# create list for member of centroid
sse=[]
for i in range(10):
    if i!=0:
    # print(centroid)
        centroid, member_of_centroid= findCentroid(data, i+1)  
        print(centroid[i])  
    # print("Pusat dengan " + str(i+1) + " klaster :"+str(centroid))

        temp_sse= getSse(data, centroid, member_of_centroid)
        sse.insert(i,temp_sse)

elbow= getRecommendCluster(sse)
print(elbow)

plt.plot([2,3,4,5,6,7,8,9,10],sse, '--bo')
plt.xlabel("Jumlah cluster")
plt.ylabel("SSE")
plt.show()
start_time = time.time()
print("--- %s seconds ---" % (time.time() - start_time))    
#     # print(i)