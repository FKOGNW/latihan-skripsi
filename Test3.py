# Test K-Means 2 Dimensi

# import array as arr # Import array lib
import random # Import random lib
import numpy as np

# inisiasi data
x= [2,10,24,12,7,8,20,1,11,23,8,4]
y= [3,20,4,21,20,21,19,21,20,31,17,30]

cluster= int(input("Masukan jumlah klaster: "))

# example centroid
centroid_x= [24, 4]
centroid_y= [4, 30]
# centroid_x= random.sample(list(x),k=cluster) #get random value from array x
# centroid_y= random.sample(list(y),k=cluster) #get random value from array y
print("Pusat Klaster awal: "+str(centroid_x)+"\n")
# print(rnd)
[float(i) for i in centroid_x]
[float(i) for i in centroid_y]
old_centroid_x=[] #temp for old centroid
old_centroid_y=[] #temp for old centroid
# [float(i) for i in old_centroid]


# create list for member of centroid
member_of_centroid_x = [[] for i in range(len(centroid_x))]
member_of_centroid_y = [[] for i in range(len(centroid_y))]

# print(member_of_centroid)

condition= "not convergen"

def kMeans(x, y, centroid_x, centroid_y, old_centroid_x, old_centroid_y, member_of_centroid_x, member_of_centroid_y):
    del member_of_centroid_x[:]    
    # create list for member of centroid
    member_of_centroid_x= [[] for i in range(len(centroid_x))]
    
    del member_of_centroid_y[:]    
    # create list for member of centroid
    member_of_centroid_y= [[] for i in range(len(centroid_y))]

    # initiate valeue of distance
    distance=[]

    # start process
    for data_x in range(len(x)):
        # print(x[data_x])
        j=0
        for data_centroid in range(len(centroid_x)):
            # print(centroid[data_centroid])
            # get value of means for every  centroid
            distance.insert(j,np.sqrt(((x[data_x] - centroid_x[data_centroid]) ** 2)+((y[data_x] - centroid_y[data_centroid]) ** 2)))   
            # print(distance[j], centroid_x[data_centroid], x[data_x], j)
            j= j+1    

        # get minimum value of means from centroid
        mini= min(distance)
        # print(mini)
        del distance[:] # clear value means data

        for data_centroid in range(len(centroid_x)):
            nilai_mini= np.sqrt(((x[data_x] - centroid_x[data_centroid]) ** 2)+((y[data_x] - centroid_y[data_centroid]) ** 2))
            # print(nilai_mini)
            if mini == nilai_mini:
                # print("data: "+ str(x[data_x]) + " anggota cluster: "+ str(centroid[data_centroid]) + " Iter: "+ str(data_centroid))
                member_of_centroid_x[data_centroid].insert(len(member_of_centroid_x[data_centroid]), x[data_x])
                member_of_centroid_y[data_centroid].insert(len(member_of_centroid_y[data_centroid]), y[data_x])
                # print(member_of_centroid_x[data_centroid])

    # print(centroid)
    # print("Klaster ke "+ str(i+1)+ " "+str(member_of_centroid_x))        

    # copy centroid to temp old_centroid
    old_centroid_x= centroid_x.copy()
    old_centroid_y= centroid_y.copy()
    # print(old_centroid)

    return (centroid_x, centroid_y, old_centroid_x, old_centroid_y, member_of_centroid_x, member_of_centroid_y)


while condition== "not convergen":
    centroid_x, centroid_y, old_centroid_x, old_centroid_y, member_of_centroid_x, member_of_centroid_y= kMeans(x, y, centroid_x, centroid_y, old_centroid_x, old_centroid_y, member_of_centroid_x, member_of_centroid_y)
    # get new centroid
    for i in range(len(centroid_x)):
        new_centroid_x= sum(member_of_centroid_x[i])/len(member_of_centroid_x[i])
        new_centroid_y= sum(member_of_centroid_y[i])/len(member_of_centroid_y[i])
        centroid_x[i]= new_centroid_x
        centroid_y[i]= new_centroid_y
        # print("Klaster X ke "+ str(i+1)+ ": "+str(member_of_centroid_x[i])) 
        # print("Pusat klaster X ke "+ str(i+1)+ ": "+str(centroid_x[i])+ "\n")
        # print("Klaster Y ke "+ str(i+1)+ ": "+str(member_of_centroid_y[i])) 
        # print("Pusat klaster Y ke "+ str(i+1)+ ": "+str(centroid_y[i])+ "\n")

        print("Klaster ke "+ str(i+1)+ ": "+str(member_of_centroid_x[i])+ ","+ str(member_of_centroid_y[i]) )
        print("Pusat klaster ke "+ str(i+1)+ ": "+str(centroid_x[i])+" " +str(centroid_y[i]) +"\n")

    if old_centroid_x==centroid_x and old_centroid_y==centroid_y :
        condition= "convergen"

    del member_of_centroid_x[:]    
    # create list for member of centroid
    member_of_centroid_x= [[] for i in range(len(centroid_x))]
    
    del member_of_centroid_y[:]    
    # create list for member of centroid
    member_of_centroid_y= [[] for i in range(len(centroid_y))]


    print(condition+ "\n" + "=================")
    kMeans(x, y, centroid_x, centroid_y, old_centroid_x, old_centroid_y, member_of_centroid_x, member_of_centroid_y) # use K-Means function