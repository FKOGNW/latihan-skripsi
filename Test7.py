# Try use silhouette for get optimal cluster

import random  # Import random lib
import time  # knowing running time program
from time import sleep

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

def findMemberCentroid(data, centroid):
    
    member_of_centroid = [[] for i in range(len(centroid))]

    distance=[]
    temp_centroid_count= 0
    temp_member=[]

    # bar = progressbar.ProgressBar(maxval=20, \
    # widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    # bar.start()
    # print(itera)
    for data_count in range(len(data)):
        mini=9999999999999999
        j=0
        for centroid_count in range(len(centroid)):
            # Get distance for each data to centroid
            distance.insert(j, np.sqrt(((data[data_count][0] - centroid[centroid_count][0]) ** 2) + ((data[data_count][1] - centroid[centroid_count][1]) ** 2)))
            
            # checking distance between centroid
            if distance[j]< mini:
                # update minimum temp value
                mini= distance[j]
                
                # get minimum distance to mark centroid
                temp_member= data[data_count]
                temp_centroid_count= centroid_count
            j+= 1
        # itera+=1    
        
        # assign member of centroid
        member_of_centroid[temp_centroid_count].insert(len(member_of_centroid[temp_centroid_count]), temp_member)
        
        # bar.update(data_count+1)
        # sleep(0.1)
    # bar.finish()
    return (member_of_centroid)

def findCentroid (data, total_centroid):   
    random.seed(200) # using seed to get constant value
    centroid= random.sample(list(data), k=total_centroid) # Get random valur from list   

    old_centroid=[[]]

    condition="not convergen"
    # print(member_of_centroid)
    max_loop=0
    while condition== "not convergen" or max_loop==1000:
        # print('.')
        member_of_centroid= findMemberCentroid(data, centroid)

        # copying centroid to old centroid
        old_centroid= centroid.copy()      
        # print(old_centroid)
        temp_x_centroid=[[] for i in range(len(old_centroid))]
        temp_y_centroid=[[] for i in range(len(old_centroid))]
        for i in range(len(old_centroid)):
            # k=0
            for j in range(len(member_of_centroid[i])):
                # print(member_of_centroid[i][j][0])
                temp_x_centroid[i].insert(j,member_of_centroid[i][j][0])
                temp_y_centroid[i].insert(j,member_of_centroid[i][j][1])
                 
                # calculate average to get new centroid 
                temp_x= sum(temp_x_centroid[i])/len(temp_x_centroid[i])
                temp_y= sum(temp_y_centroid[i])/len(temp_y_centroid[i])
                centroid[i]= [temp_x,temp_y]
                # print(old_centroid)
                # centroid[i][1]= temp_y

            #     # print(str(old_centroid)+ str(i)) 
            #      
            # print("Klaster ke "+ str(i+1)+ ": "+str(member_of_centroid[i])) 
            # print("Pusat klaster "+ str(i+1)+" baru "+  ": "+str(centroid[i])+ "\n")
        # print(old_centroid)
        print("Iteration "+ str(max_loop+1), end="\r")
        max_loop+=1    
        if old_centroid== centroid or max_loop== 1000:
            condition= "convergen"
            print(str(max_loop+1) + " iteration\n" + condition + "\n==========\n")
            return centroid, member_of_centroid

# data= [[2,3], [10,20], [24,4], [12,21], [7,20], [8,21], [20,19], [1,21], [11,20], [23,31], [8,17], [4,30],[1,4]]
# print(X.tolist())
# print(len(X))

# centroid, member_of_centroid= findCentroid(data, 8)
# print(centroid)   
def silhouette_calc(centroid, member_of_centroid):
    # print(1)
    silhouette=[]

    average_distance_intra=[]
    average_distance_extra=[]
    for i in range(len(centroid)):
        # nearest_centroid=0
        distance=[]
        # measure distance for internal centroid
        for j in range(len(member_of_centroid[i])):
            distance.insert(j, np.sqrt(((member_of_centroid[i][j][0] - centroid[i][0])**2) + ((member_of_centroid[i][j][1] - centroid[i][1])**2)))
            # print(member_of_centroid[i][j][0])
            # print(centroid[i][0])
        # Getting average distance internal centroid    
        average_distance_intra.insert(i, sum(distance)/ len(distance))
        # print(member_of_centroid[i])
        # print(distance)


        # nearest=999999999999999
        # # Knowing nearest centroid
        # for k in range(len(centroid)):
        #     if k != i:
        #         distance_between_centroid= np.sqrt(((centroid[k][0] - centroid[i][0])**2) + ((centroid[k][1] - centroid[i][1])**2))
        #         # print(distance_between_centroid)
        #         # print(k)
        #         if(distance_between_centroid< nearest):
        #             nearest= distance_between_centroid
        #             nearest_centroid= k
        # # print(nearest_centroid)    
                
        # distance_extra=[]
        # # measure distance from member of nearest centroid
        # for l in range(len(member_of_centroid[nearest_centroid])):
        #     distance_extra.insert(l, np.sqrt((member_of_centroid[nearest_centroid][l][0] - centroid[i][0])**2) + ((member_of_centroid[nearest_centroid][l][1] - centroid[i][1])**2))
        #     # print(nearest)
        # average_distance_extra.insert(i, sum(distance_extra)/ len(distance_extra))    
    # print(average_distance_intra)
    # print(average_distance_extra)
    # for i in range(len(centroid)):
        temp_average=[]
        for k in range(len(centroid)):
            temp_distance=[]
            if k != i:
                for j in range(len(member_of_centroid[k])):
                    temp_distance.insert(k, np.sqrt(((member_of_centroid[k][j][0] - centroid[i][0])**2) + ((member_of_centroid[k][j][1] - centroid[i][1])**2)))
                    # print(temp_distance)
                temp_average.insert(k, sum(temp_distance)/ len(temp_distance))

        # print(temp_average)    
        average_distance_extra.insert(i,min(temp_average))
        # print(average_distance_extra)
    for i in range(len(centroid)):
        silhouette.insert(i, (average_distance_extra[i]-average_distance_intra[i])/ max(average_distance_extra[i], average_distance_intra[i]))
    #     print(i)
    # print(silhouette)

    return max(silhouette)

start_time = time.time()
df_loc= pd.read_csv("data/databike.csv", dtype={"start station latitude": float, "start station longitude": float})
df_loc.head()

X = df_loc[['latitude', 'longitude']].values
data= X.tolist()


print("Start Program...")
silhouette=[]
for count in range(3):
    if count!= 0:
        print("Start with " + str(count+1)+ " cluster...")
        centroid, member_of_centroid= findCentroid(data, count+1)
        silhouette.insert(count, silhouette_calc(centroid, member_of_centroid))
        # print(silhouette)
        print(centroid)
    # print(count)

print(silhouette)
print("Finish")    
print(silhouette.index(max(silhouette))+2)





print("--- %s seconds ---" % (time.time() - start_time)) 

    # print(centroid[1])