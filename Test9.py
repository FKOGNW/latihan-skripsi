 # clustering dataset
# determine k using elbow method

from sklearn.cluster import KMeans
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
import matplotlib.pyplot as plt

# x1 = np.array([5, 10, 15, 24, 30, 85, 71, 60, 55, 80])
# x2 = np.array([3, 15, 12, 10, 45, 70, 80, 78, 52, 91])

x1 = np.array([5, 10, 24, 12, 7, 8, 20, 1, 11, 23, 8, 4, 10, 21, 9, 9, 22, 20, 9, 21])
x2 = np.array([3, 15, 4, 21, 20, 21, 19, 21, 20, 31, 17, 30, 9, 23, 19, 8, 11, 9, 12,4])
data= [[5,3], [10,15], [24,4], [12,21], [7,20], [8,21], [20,19], [1,21], [11,20], [23,31], [8,17], [4,30],
        [10,9], [21,23], [9,19], [9,8], [22,11], [20,9], [9,12], [21,4]]
# data= [[5,3], [10,15], [24,4], [12,21], [7,20], [8,21], [20,19], [1,21], [11,20], [23,31], [8,17], [4,30],
#         [10,9], [21,23], [3,8], [1,2], [27,17], [23,19], [5,10], [13,3]]


plt.plot()
plt.xlim([0, 10])
plt.ylim([0, 10])
plt.title('Dataset')
# plt.scatter(x1, x2)
plt.show()

# create new plot and data
plt.plot()
X = np.array(data).reshape(len(x1), 2)
colors = ['b', 'g', 'r']
markers = ['o', 'v', 's']

# k means determine k
distortions = []
K = range(2,11)
for k in K:
    kmeanModel = KMeans(n_clusters=k).fit(X)
    kmeanModel.fit(X)
    distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])

# Plot the elbow
plt.plot(K, distortions, 'bx-')
plt.xlabel('k')
plt.ylabel('Distortion')
plt.title('The Elbow Method showing the optimal k')
plt.show()

 